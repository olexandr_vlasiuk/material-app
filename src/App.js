import React, { Component } from 'react'
import HomePage from './components/home/home'
import LoginPage from './components/login/login'
import { createMuiTheme } from '@material-ui/core/styles'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider'
import './App.css'

const theme = createMuiTheme({});

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <MuiThemeProvider theme={theme}>
          <BrowserRouter>
            <Switch>
              <Route path="/" exact component={HomePage} />
              <Route path="/login" exact component={LoginPage} />
              <Route component={LoginPage} />
            </Switch>
          </BrowserRouter>
        </MuiThemeProvider>        
      </React.Fragment>
    );
  }
}

export default App;
