import React from 'react'
import PropTypes from 'prop-types'
import Header from '../header/header'
import WordCloud from 'react-d3-cloud'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import data from './words'

const styles = () => ({
    d3Cloud: {
        display: 'flex',
        justifyContent: 'center'
    },
    mainTitle: {
        marginLeft: '5%'
    }
})

class HomePage extends React.Component {

    fontSizeMapper = word => Math.log2(word.value) * 5;
    rotate = word => word.value % 360;

    render() {
        const { classes } = this.props

        return (
            <React.Fragment>
                <Header />
                <br/>
                <Typography 
                    variant="h5" 
                    className={classes.mainTitle}>
                    Most common words in tweets:
                </Typography>
                <div className={classes.d3Cloud}>                    
                    <WordCloud
                        data={data}
                        fontSizeMapper={this.fontSizeMapper}
                        rotate={this.rotate}
                        width={window.screen.width}
                    />
                </div>                
            </React.Fragment>            
        )
    }
}

HomePage.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(HomePage);