import React from 'react'
import {
    Paper,
    Button,
    TextField,
    withStyles
} from '@material-ui/core'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'


import logo from '../../logo.svg'

const styles = () => ({
    loginContainer: {
        height: '100%',
        display: 'flex'
    },
    loginForm: {
        margin: 'auto',
        display: 'flex',
        padding: '40px',
        alignItems: 'center',
        flexDirection: 'column'
    }
})

class LoginPage extends React.Component {

    render() {
        const { classes } = this.props

        return (
            <div className={classes.loginContainer}>
                <Paper className={classes.loginForm}>
                    <img 
                        src={logo} 
                        width={200} 
                        alt="Logo" />
                    <TextField  
                        fullWidth
                        placeholder="Login"
                        margin="normal" />
                    <TextField 
                        fullWidth
                        margin="normal"
                        placeholder="Password"
                        type="password"/>
                    <Button 
                        fullWidth
                        variant="contained" 
                        color="primary"
                        component={Link}
                        to="/">
                        Log In</Button>                            
                </Paper> 
            </div>            
        )
    }
}

LoginPage.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(LoginPage);